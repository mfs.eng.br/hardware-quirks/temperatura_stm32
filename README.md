# Monitoramento Ambiental com STM32 Nucleo L476RG e DHT22

Este projeto visa desenvolver um sistema de monitoramento de temperatura e umidade utilizando o microcontrolador STM32 Nucleo L476RG e o sensor DHT22. A informação é exibida em tempo real através de um display TFT, com registro de valores máximos e mínimos e uma representação gráfica das variações ao longo das últimas 24 horas.

## Objetivo

O sistema desenvolvido permite o monitoramento contínuo das condições ambientais, essencial para aplicações em áreas como agricultura e residências inteligentes. Este repositório contém todo o código-fonte e as instruções necessárias para replicar o projeto.

## Materiais Necessários

- STM32 Nucleo L476RG
- Sensor de temperatura e umidade DHT22
- Display TFT MCUFRIEND_kbv

## Configuração

### Hardware

1. Conecte o DHT22 ao STM32:
   - VCC ao pino 3.3V
   - GND ao pino GND
   - DHTPIN (PB7) ao pino de dados do DHT22

2. Conecte o Display TFT:
   - LCD_CS (A3), LCD_CD (A2), LCD_WR (A1), LCD_RD (A0), LCD_RESET (A4) aos respectivos pinos na STM32

### Software

Este projeto utiliza o VSCode com PlatformIO. As bibliotecas necessárias estão especificadas no arquivo `platformio.ini` incluído neste repositório.

## Execução

Para rodar o projeto:
1. Clone o repositório.
2. Abra o projeto no VSCode com o PlatformIO instalado.
3. Compile e faça o upload do código para a placa STM32 Nucleo L476RG.

## Funcionalidades

- Monitoramento contínuo de temperatura e umidade.
- Exibição de dados em tempo real com valores mínimo e máximo.
- Representação gráfica das variações de temperatura e umidade.

## Contribuições

Contribuições são bem-vindas. Para contribuir, por favor, faça um fork do repositório, faça suas alterações e envie um pull request.

## Licença

Este projeto é distribuído sob a licença MIT. Veja o arquivo `LICENSE` para mais detalhes.

## Contato

[mfs.eng.br](https://mfs.eng.br/2024/04/28/monitoramento-ambiental-com-stm32-nucleo-l476rg-e-dht22-um-guia-pratico/) - Sobre esse projeto no meu Blog.
